# EAI: 2021
# Nombre del Alumno: Daniel Nahuen Zalasar
# DNI: 41654010

import os

empleados = [[100, "Steven King", "king@gmail.com", "Gerencia", 24000],
             [101, "Neena Kochhar", "neenaKochhar@gmail.com", "Ventas", 17000],
             [102, "Lex De Haan", "lexDeHaan@gmail.com", "Compras", 16000],
             [103, "Alexander Hunold", "alexanderHunold@gmail.com", "Compras", 9000],
             [104, "David Austin", "davidAustin@gmail.com", "Compras", 4800],
             [105, "Valli Pataballa", "valliPataballa@gmail.com", "Ventas", 4200],
             [106, "Nancy Greenberg", "nancyGreenberg@gmail.com", "Ventas", 5500],
             [107, "Daniel Faviet", "danielFaviet@gmail.com", "Ventas", 3000],
             [108, "John Chen", "johnChen@gmail.com", "Compras", 8200],
             [109, "Ismael Sciarra", "ismaelSciarra@gmail.com", "Compras", 7700],
             [110, "Alexander Khoo", "alexanderKhoo@gmail.com", "Comrpas", 3100]
            ]
# A continuación un ejemplo que muestra la lista. Puede borrarlo            
#for e in empleados:    
#    print(e)

def continuar():
    print()
    input('presione una tecla para continuar ...')
    os.system('cls')


def menu():
    print('1) Modificar un empleado')
    print('2) Mostrar el salario de los empleados')
    print('3) Eliminar un empleado')
    print('4) Ordenar')
    print('5) Salir')


    eleccion = int(input('Elija una opcion: '))
    while not((eleccion >= 1) and (eleccion <= 5)):
        eleccion = int(input('Elija una opcion: '))
    os.system('cls')
    return eleccion


def ordenar_ascendente(empleados):
    empleados.sort() #ordena de forma ascendete
    print(empleados)


def buscarporid(empleados):
    id = int(input('ID: '))
    datos = empleados.get(dni,-1)
    return id,datos



def modificar(empleados):
    for e in empleados.value:    
    print(e)                #Muestro los datos actuales de los empleados

    print('Modificar datos de un Empleado')
    id, datos = buscarporid(empleados)    
    if (datos != -1):

        nombre = input('Nombre: ')
        email= input('Email: ')
        departamento= input('Departamento: ')

        empleados[id] = [nombre,email,departamento]
        print('Modificado Exitosamente')
    else:
        print('El estudiante no existe')


def mostrar(empleados):
    print('Listado de empleados')
    for valor in empleados.value():
        print(valor)

def eliminar(empleados):
    print('Eliminar un Empleado')
    dni, datos = buscarporid(empleados)    
    if (datos != -1):
        print('Se va a eliminar: ', empleados[id])
        confirmacion = input('¿Confirma eliminarlo? s/n: ')
        if (confirmacion == 's'):
            del empleados[id]
            print ('Empleado eliminado.')
    else:
        print('El ID no existe') 


def salir():
    print('Fin del programa...')


#principal
opcion = 0
os.system('cls')
while (opcion != 5):
    opcion = menu()
    if opcion == 1:
        modificar(empleados)
    elif opcion == 2:
        mostrar(empleados)
    elif opcion == 3:
        eliminar(empleados)
    elif opcion == 4:
        ordenar_ascendente(empleados)
    elif (opcion == 5):
        salir()        